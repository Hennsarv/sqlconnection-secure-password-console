﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Security;
namespace ConsoleApp22
{
    class Program
    {
        static void Main(string[] args)
        {
            Northwind22Entities ne = new Northwind22Entities();
            SqlConnection conn = (SqlConnection)ne.Database.Connection;
            Console.Write("Kes sa oled: ");
            string userName = Console.ReadLine();
            //Console.Write("Mis on su parool:");
            //string password = Console.ReadLine();
            #region MyRegion
            //SecureString securePassword = new SecureString();
            StringBuilder sb = new StringBuilder();
            ConsoleKeyInfo key;
            Console.Write("Parool: ");
            do
            {
                key = Console.ReadKey(true);

                // Ignore any key out of range.
                //if (((int)key.Key) >= 65 && ((int)key.Key <= 90))
                if (key.Key != ConsoleKey.Enter)
                {
                    // Append the character to the password.
                    sb.Append(key.KeyChar);
                    Console.Write("*");
                }
                // Exit if Enter key is pressed.
            } while (key.Key != ConsoleKey.Enter);
            Console.WriteLine();
            Console.WriteLine(sb.ToString());
            #endregion

            string password = sb.ToString();


            conn.ConnectionString =
            (new SqlConnectionStringBuilder(conn.ConnectionString)
            {
                
                UserID = userName, // "Test",
                Password = password // "Pa$$w0rd"
            }).ToString();
            try
            {
                conn.Open();

                foreach (var x in ne.Categories)
                    Console.WriteLine(x.CategoryName);

            }
            catch
            {
                Console.WriteLine("Sind ei teenindata");
            }
            finally
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();
            }
        }
    }
}
